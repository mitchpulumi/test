"""An AWS Python Pulumi program"""

import pulumi
import pulumi_random as random

# Create an AWS resource (S3 Bucket)
rando = random.RandomPet("my-rando-pet")
# Export the name of the bucket
pulumi.export('random pet', rando.id)
